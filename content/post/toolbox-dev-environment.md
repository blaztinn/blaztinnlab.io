---
title: "Developing inside a Toolbox container"
date: 2020-05-07T18:20:10+02:00
description: "."
tags: ["toolbox", "container", "fedora", "dnf", "mesa"]
keywords: ["toolbox", "container", "fedora", "dnf", "mesa"]
draft: false
---

When I wanted to make a patch for some library, I always _disliked_ installing all of the __build dependencies__ on my system and __messing__ with `LD_LIBRARY_PATH` ([link](https://www.hpc.dtu.dk/?page_id=1180)) to be able build and run the patched library.
I never took time to uninstall build dependencies afterwards and `LD_LIBRARY_PATH` is a step that I would've liked to skip when possible.

If you are having similar issues and are using Fedora then you can use the Toolbox containers to avoid these issues.

## Toolbox Containers

The [Toolbox](https://github.com/containers/toolbox) repo describes the software as:

> Toolbox is a tool that offers a familiar package based environment for developing and debugging software that runs fully unprivileged using Podman.
>
> The toolbox container is a fully mutable container; when you see `yum install ansible` for example, that's something you can do inside your toolbox container, without affecting the base operating system.

Basically the Toolbox container is its own environment that can still access your home folder, display, dbus, etc. You can install packages in it with regular `dnf install vim` and run programs on your host desktop.

## Example Development Workflow

To show an example of developing with Toolbox lets say we have a program `foo` that is having an issue and we suspect the driver in the [Mesa](https://www.mesa3d.org/) libs to be the culprit.

First thing we need to do is to __create a container__.

``` sh
$ toolbox create -c mesa-devel
```

This will create a container named `mesa-devel` somewhere in the hidden folders in your home directory.

We can then __enter__ the container.

``` sh
$ toolbox enter mesa-devel
⬢$
```

Notice how the prompt starts with `⬢`, this is a clue that you are inside a container so that you don't mix it up with you host environment.

If we now try to run our program `foo` in the container (our home folder is readable/writable) it will not run since it depends on the Mesa libs that are not installed in it. To fix this we __install__ them as we would on a host.

``` sh
⬢$ sudo dnf install mesa-dri-drivers
```

Now the program runs but we can't debug the driver, we are missing the debug symbols. We need to install the __debug__ package.

``` sh
⬢$ sudo dnf debuginfo-install mesa-dri-drivers
```

This is the first thing that would clutter our system but now it is __installed in a container__!

With any luck we find the code that could be causing our issue, so we clone the source code repo to make a patch. But before we can build that code we need to install its __build dependencies__.

``` sh
⬢$ sudo dnf builddep --skip-unavailable mesa
```

After we build the libs we can install them directly to container's `/` without a package manager.
If you care enough (and you probably should unless this is a throwaway container) about not overwriting the installed files from the `mesa-dri-drivers` package uninstall that package first.

Now running the program inside the container with just `./foo` (__no `LD_LIBRARY_PATH` needed!__) will use the newly compiled libs, while running it in a host terminal will use the system libs. And both will run on the same desktop since the display is shared!
Of course this only works if the API/ABI of the lib stays the same, otherwise we need to compile the program for each environment.

_All_ that is left now is to patch the driver code.

When we are done with that we can __exit__ the container like exiting a normal shell.

``` sh
⬢$ exit
$
```

The installed packages in a container persist so we can enter it anytime later.

If we don't need the container any more we can safely __delete__ it.

``` sh
$ toolbox rm mesa-devel
```

This way our system stays clean while the upstream hopefully got an [useful patch](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/4925).

What I really like about this workflow is that all the development is done inside a container! There is __no need to track__ what packages we have installed in order to remove them later, we can install __different version__ of the packages to check for regressions, we can also install files __directly to root__ or do other unsafe things without the fear of breaking our main system,... This opens up a lot of possibilities.


## A Possible Improvement

A good extension to this workflow would be to use `fedpkg`([link](https://fedoraproject.org/wiki/Package_maintenance_guide#anon)) that uses the `rpm` package of the lib to build and install it.
This way we would be compiling the lib with the __same compile flags as used by the distro__ which can save us a lot of time of manually configuring the build options. The result would also be a `rpm` which can be __installed and uninstalled__ with `dnf`.

To do this we probably just need to point the package's `spec` file to our local clone of the source code. Or maybe we can even point it directly to upstream repo and let the `rpm` tools clone it, if we can of course change the source code afterwards.

Let this be an exercise for the reader.